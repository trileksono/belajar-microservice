package com.example.tri.transactionservice.service;

import com.example.tri.transactionservice.model.Member;
import com.example.tri.transactionservice.service.impl.MemberServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "member-service", fallback = MemberServiceImpl.class)
public interface MemberService {

  @GetMapping(value = "/member/{id}")
  Member getById(@PathVariable("id") Long id);

}
