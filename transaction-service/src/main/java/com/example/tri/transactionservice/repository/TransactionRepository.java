package com.example.tri.transactionservice.repository;

import com.example.tri.transactionservice.model.Transactions;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transactions, String> {
}
