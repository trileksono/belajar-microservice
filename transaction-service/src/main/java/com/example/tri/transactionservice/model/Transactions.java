package com.example.tri.transactionservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "transactions")
@Data
public class Transactions {

  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  @Column(name = "id")
  private String id;

  @Column(name = "id_member")
  @NotNull
  private Long idMember;

  @Column(name = "id_buku")
  @NotNull
  private Long idBuku;

  @Column(name = "tgl_peminjaman")
  @Temporal(TemporalType.DATE)
  @JsonFormat(pattern = "dd-MM-yyyy")
  private Date tglPeminjaman;

  @Column(name = "tgl_pengembalian")
  @Temporal(TemporalType.DATE)
  @JsonFormat(pattern = "dd-MM-yyyy")
  private Date tglPengembalian;
}
