package com.example.tri.transactionservice.controller;

import com.example.tri.transactionservice.model.Transactions;
import com.example.tri.transactionservice.service.TransactionService;
import com.google.gson.Gson;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

  @Autowired
  private TransactionService mTransactionService;

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Transactions save(@RequestBody @Valid Transactions transaction) {
    return mTransactionService.save(transaction);
  }

  @DeleteMapping
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@RequestBody String id) {
    mTransactionService.delete(id);
  }

  @GetMapping
  public Page<Transactions> findAll(Pageable pageable) {
    return mTransactionService.findAll(pageable);
  }

  @GetMapping("/{id}")
  public Transactions getById(@PathVariable("id") Transactions transaction) {
    return transaction;
  }

  @PutMapping("/{id}")
  public Transactions update(@PathVariable("id") Transactions transaction, @Valid @RequestBody Transactions transactionUpdate) {
    return mTransactionService.update(transaction, transactionUpdate);
  }
}
