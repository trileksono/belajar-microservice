package com.example.tri.transactionservice.service.impl;

import com.example.tri.transactionservice.model.Book;
import com.example.tri.transactionservice.model.Member;
import com.example.tri.transactionservice.model.Transactions;
import com.example.tri.transactionservice.repository.TransactionRepository;
import com.example.tri.transactionservice.service.BookService;
import com.example.tri.transactionservice.service.MemberService;
import com.example.tri.transactionservice.service.TransactionService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@Log4j2
public class TransactionImpl implements TransactionService {

  @Autowired
  private TransactionRepository mTransactionRepository;

  @Autowired
  private MemberService mMemberService;

  @Autowired
  private BookService mBookService;

  @Override
  public Transactions save(Transactions transaction) {
    Book book = mBookService.getById(transaction.getIdBuku());
    Member member = mMemberService.getById(transaction.getIdMember());
    if (book == null || member == null) {
      throw new RuntimeException("Buku atau member tidak ditemukan");
    }
    log.info(transaction.getIdBuku());
    log.info(transaction.getIdMember());
    return mTransactionRepository.save(transaction);
  }

  @Override
  public void delete(String id) {
    mTransactionRepository.deleteById(id);
  }

  @Override
  public Page<Transactions> findAll(Pageable pageable) {
    return mTransactionRepository.findAll(pageable);
  }

  @Override
  public Transactions update(Transactions transaction, Transactions transactionUpdate) {
    Book book = mBookService.getById(transaction.getIdBuku());
    Member member = mMemberService.getById(transaction.getIdMember());
    if (book == null || member == null) {
      throw new RuntimeException("Buku atau member tidak ditemukan");
    }
    transactionUpdate.setId(transaction.getId());
    BeanUtils.copyProperties(transactionUpdate, transaction);
    return mTransactionRepository.save(transaction);
  }
}
