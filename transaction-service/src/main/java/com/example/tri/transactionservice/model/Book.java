package com.example.tri.transactionservice.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Book {
  private Long idBook;
  private String namaBuku;
  private String pengarang;
}
