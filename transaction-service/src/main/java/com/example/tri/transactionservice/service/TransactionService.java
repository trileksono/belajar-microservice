package com.example.tri.transactionservice.service;

import com.example.tri.transactionservice.model.Transactions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TransactionService {

  Transactions save(Transactions transaction);

  void delete(String id);

  Page<Transactions> findAll(Pageable pageable);

  Transactions update(Transactions transaction, Transactions transactionUpdate);
}
