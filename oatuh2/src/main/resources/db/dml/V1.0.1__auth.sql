INSERT INTO oauth_client_details (access_token_validity,additional_information,authorities,authorized_grant_types,autoapprove,client_id,client_secret,refresh_token_validity,resource_ids,`scope`,web_server_redirect_uri) VALUES
(3600,'{"additional_param":"hello OAuth2"}','CLIENT','password,refresh_token,client_credentials',1,'clientid','$2a$06$eMfAQrgiQDRRSJCT9z8pz.oxs0igB.CL8zR20USQC.9.mTsVVaASy',2592000,'PEMINJAMAN','read,write',' ');

INSERT INTO users (id,is_active,password,username) VALUES
('1',1,'$2a$06$eMfAQrgiQDRRSJCT9z8pz.oxs0igB.CL8zR20USQC.9.mTsVVaASy','tri');

INSERT INTO roles (id,roles) VALUES
('1','ADMIN');