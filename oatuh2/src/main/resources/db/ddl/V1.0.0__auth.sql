CREATE TABLE IF NOT EXISTS `oauth_client_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `access_token_validity` int(11) NOT NULL,
  `additional_information` varchar(255) DEFAULT NULL,
  `authorities` varchar(255) NOT NULL,
  `authorized_grant_types` varchar(255) NOT NULL,
  `autoapprove` smallint(6) DEFAULT NULL,
  `client_id` varchar(255) NOT NULL,
  `client_secret` varchar(255) NOT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `resource_ids` varchar(255) NOT NULL,
  `scope` varchar(255) NOT NULL,
  `web_server_redirect_uri` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(255) NOT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_UN` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `roles` (
  `id` varchar(255) NOT NULL,
  `roles` varchar(255) DEFAULT NULL,
  KEY `FKfwd8notqt2avggaputa06qln5` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

