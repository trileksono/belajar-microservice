package com.example.tri.oatuh2.repository;

import com.example.tri.oatuh2.model.OauthClientDetails;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface Oauth2ClientDetailRepository extends PagingAndSortingRepository<OauthClientDetails, Long> {

  Optional<OauthClientDetails> findByClientId(String clientId);

}
