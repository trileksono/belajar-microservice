package com.example.tri.oatuh2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class Oatuh2Application {

  public static void main(String[] args) {
    SpringApplication.run(Oatuh2Application.class, args);
  }

}
