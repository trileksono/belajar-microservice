package com.example.tri.oatuh2.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "oauth_client_details")
@Data
public class OauthClientDetails implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Long id;
  @Basic(optional = false)
  @Column(name = "client_id")
  private String clientId;
  @Basic(optional = false)
  @Column(name = "resource_ids")
  private String resourceIds;
  @Basic(optional = false)
  @Column(name = "client_secret")
  private String clientSecret;
  @Basic(optional = false)
  @Column(name = "scope")
  private String scope;
  @Basic(optional = false)
  @Column(name = "authorized_grant_types")
  private String authorizedGrantTypes;
  @Column(name = "web_server_redirect_uri")
  private String webServerRedirectUri;
  @Basic(optional = false)
  @Column(name = "authorities")
  private String authorities;
  @Basic(optional = false)
  @Column(name = "access_token_validity")
  private int accessTokenValidity;
  @Column(name = "refresh_token_validity")
  private Integer refreshTokenValidity;
  @Column(name = "additional_information")
  private String additionalInformation;
  @Column(name = "autoapprove")
  private Short autoapprove;
}