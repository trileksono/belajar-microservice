package com.example.tri.oatuh2.config;

import com.example.tri.oatuh2.model.OauthClientDetails;
import com.example.tri.oatuh2.repository.Oauth2ClientDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.exceptions.InvalidClientException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ClientDetailService implements ClientDetailsService {

  @Autowired
  private Oauth2ClientDetailRepository mOauth2ClientRepository;


  @Override
  public ClientDetails loadClientByClientId(String s) throws ClientRegistrationException {
    Optional<OauthClientDetails> optionalClient = mOauth2ClientRepository.findByClientId(s);
    if (!optionalClient.isPresent()) {
      throw new InvalidClientException("Client not found");
    }
    OauthClientDetails clientDetails = optionalClient.get();
    return new BaseClientDetails(clientDetails.getClientId(),
            clientDetails.getResourceIds(),
            clientDetails.getScope(),
            clientDetails.getAuthorizedGrantTypes(),
            clientDetails.getAuthorities());
  }
}
