package com.example.tri.oatuh2.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author skyvision
 */
@Entity
@Table(name = "users")
@Data
public class Users implements Serializable {

  private static final long serialVersionUID = 1L;
  @Column(name = "is_active")
  Boolean isActive;
  @Id
  @Basic(optional = false)
  @Column(name = "id")
  private String id;
  @Basic(optional = false)
  @Column(name = "username")
  private String username;
  @Column(name = "password")
  private String password;
  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "roles", joinColumns = @JoinColumn(name = "id"))
  @Column(name = "roles")
  private List<String> roles;

}
