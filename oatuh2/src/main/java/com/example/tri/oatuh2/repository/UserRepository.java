package com.example.tri.oatuh2.repository;

import com.example.tri.oatuh2.model.Users;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<Users, String> {

  Users findByUsername(String username);

}
