package com.example.tri.oatuh2.config;

import com.example.tri.oatuh2.model.Users;
import com.example.tri.oatuh2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class UsersDetailService implements UserDetailsService {

  @Autowired
  private UserRepository mUserRepository;

  @Override
  public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
    Users users = mUserRepository.findByUsername(s);
    if (users == null) {
      throw new UsernameNotFoundException("Username tidak ditemukan");
    }
    Collection<GrantedAuthority> grandtedUser = new ArrayList<>();
    for (String role : users.getRoles()) {
      grandtedUser.add(new SimpleGrantedAuthority(role));
    }

    return new User(
            users.getUsername(),
            users.getPassword(),
            users.getIsActive(),
            true,
            true,
            true,
            grandtedUser
    );
  }
}
