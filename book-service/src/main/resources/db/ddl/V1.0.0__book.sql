CREATE TABLE IF NOT EXISTS buku(
    id_buku BIGINT NOT NULL AUTO_INCREMENT,
    nama_buku varchar(255) NOT NULL,
    pengarang varchar(255) NOT NULL,
    CONSTRAINT BUKU_PK PRIMARY KEY (id_buku)
);