package com.example.tri.bookservice.service;

import com.example.tri.bookservice.entity.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BookService {

  Book save(Book book);

  void delete(Long id);

  Page<Book> findAll(Pageable pageable);

  Book update(Book book, Book bookUpdate);

}
