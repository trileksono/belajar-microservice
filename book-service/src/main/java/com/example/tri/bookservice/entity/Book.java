package com.example.tri.bookservice.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "buku")
@Data
public class Book {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_buku")
  private Long idBook;

  @Column(name = "nama_buku")
  @Size(min = 3, max = 200)
  private String namaBuku;

  @Column(name = "pengarang")
  @NotNull
  private String pengarang;
}
