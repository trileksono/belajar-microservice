CREATE TABLE IF NOT EXISTS member(
    id_member BIGINT NOT NULL AUTO_INCREMENT,
    nama varchar(255) NOT NULL,
    alamat varchar(255) NOT NULL,
    CONSTRAINT member_PK PRIMARY KEY (id_member)
);