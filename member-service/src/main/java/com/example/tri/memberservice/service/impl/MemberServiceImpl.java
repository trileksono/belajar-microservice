package com.example.tri.memberservice.service.impl;

import com.example.tri.memberservice.entity.Member;
import com.example.tri.memberservice.repo.MemberRepository;
import com.example.tri.memberservice.service.MemberService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class MemberServiceImpl implements MemberService {

  @Autowired
  private MemberRepository mMemberRepository;

  @Override
  public Member save(Member member) {
    return mMemberRepository.save(member);
  }

  @Override
  public void delete(Long id) {
    mMemberRepository.deleteById(id);
  }

  @Override
  public Page<Member> findAll(Pageable pageable) {
    return mMemberRepository.findAll(pageable);
  }

  @Override
  public Member update(Member member, Member memberUpdate) {
    if (member == null) {
      throw new EntityNotFoundException();
    }
    memberUpdate.setIdMember(member.getIdMember());
    BeanUtils.copyProperties(memberUpdate, member);
    return mMemberRepository.save(member);
  }
}
