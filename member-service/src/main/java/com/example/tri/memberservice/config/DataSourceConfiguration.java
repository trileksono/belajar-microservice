package com.example.tri.memberservice.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfiguration {

  @Autowired
  private Environment env;

  @Bean
  DataSource mDataSource() {
    HikariDataSource dataSource = new HikariDataSource();
    dataSource.setDriverClassName(env.getProperty("spring.db.driverClassName"));
    dataSource.setJdbcUrl(env.getProperty("spring.db.url"));
    dataSource.setUsername(env.getProperty("spring.db.username"));
    dataSource.setPassword(env.getProperty("spring.db.password"));
    dataSource.setMaximumPoolSize(Integer.parseInt(env.getProperty("spring.db.max-pool-size")));
    dataSource.setMinimumIdle(Integer.parseInt(env.getProperty("spring.db.min-idle")));
    dataSource.setIdleTimeout(Long.parseLong(env.getProperty("spring.db.idle-timeout")));
    dataSource.setMaxLifetime(Long.parseLong(env.getProperty("spring.db.max-life-time")));
    dataSource.addDataSourceProperty("cachePrepStmts", true);
    dataSource.addDataSourceProperty("prepStmtCacheSize", 250);
    dataSource.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
    return dataSource;
  }
}
