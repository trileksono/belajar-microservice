package com.example.tri.memberservice.service;

import com.example.tri.memberservice.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MemberService {

  Member save(Member member);

  void delete(Long id);

  Page<Member> findAll(Pageable pageable);

  Member update(Member member, Member memberUpdate);
}
